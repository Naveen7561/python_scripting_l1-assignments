def pascals_triangle(n_rows):
    results = [] 
    for _ in range(n_rows): 
        row = [1] 
        if results:
            last_row = results[-1] 
            row.extend([sum(pair) for pair in zip(last_row, last_row[1:])])
           
            row.append(1)
        results.append(row) 
    return results
if __name__=="__main__":
    n=int(input())
    l=pascals_triangle(n)
    l.reverse() 
    ns=0
    for i in l:
        print(ns*" ",end=" ")
        for j in i:
            print(j,end=" ")
        print()
        ns+=1
    
